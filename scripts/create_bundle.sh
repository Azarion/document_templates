#!/bin/bash

echo "creating tgz for bundling"


TMPDIR=/tmp

SCRIPTSTMP=`mktemp -d -p "$TMPDIR"`

echo "creating scripts tgz"
echo " - using $SCRIPTSTMP"
cp scripts/build_pdf.sh  $SCRIPTSTMP/
cp scripts/test_pdf.sh   $SCRIPTSTMP/
tar czvf doc_scripts-$CI_COMMIT_TAG.tgz  -C $SCRIPTSTMP .
rm -r $SCRIPTSTMP

UCLTMP=`mktemp -d -p "$TMPDIR"`

echo "creating ucl template tgz"
echo " - using $UCLTMP"
cp template.tex $UCLTMP/
cp -r images $UCLTMP
tar czvf ucl_template-$CI_COMMIT_TAG.tgz -C $UCLTMP .
rm -r $UCLTMP
